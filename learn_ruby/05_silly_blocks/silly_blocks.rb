def reverser
  yield.split(' ').map { |word| word.reverse }.join(' ')
end

def adder val = 1
  yield + val
end

def repeater num = 2
  num.times { yield }
end