def translate phrase
  phrase.split(' ').map { |word| translate_word(word) }.join(' ')
end
  
def translate_word word
  vowels = ['a', 'e', 'i', 'o', 'u']
  
  pos = 0
  
  until (vowels.include?(word[pos]) and word[pos - 1] != 'q') or pos == word.length
    pos += 1
  end
  
  word = word[pos...word.length] << word[0...pos]  
  word << "ay"
end