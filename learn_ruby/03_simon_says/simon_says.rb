def echo phrase
  phrase
end

def shout phrase
  phrase.upcase
end

def repeat phrase, times = 2
  ([phrase] * times).join(" ")
end

def start_of_word word, num_letterse
  word[0...num_letters]
end

def first_word phrase
  phrase.split(" ")[0]
end

def titleize phrase
  little_words = ['and', 'the', 'over']
  phrase = phrase.split(" ").map { |word, ind| little_words.include?(word) ? word : word.capitalize }
  phrase[0].capitalize!
  phrase.join(" ")
end