def add a, b
  a + b
end

def subtract a, b
  a-b
end

def sum numbers
  (numbers << 0).inject(:+)
end

def multiply numbers
  numbers.inject(:*)
end

def power(base, exponent) #doesn't handle decimal exponents
  exponent > 1 ? ([base]*exponent).inject(1,:*) : 1.0/([base]*(-1 * exponent)).inject(1,:*)
end

def factorial num
  return nil if num < 0
  (1..num+1).inject(:*)/(num+1)
end
