class Dictionary
  attr_accessor :d
  
  def initialize
    @dictionary = {}
  end
  
  def entries
    @dictionary
  end
  
  def add new_word
    new_word.is_a?(Hash) ? @dictionary.merge!(new_word) : @dictionary[new_word] = nil
  end
  
  def keywords
    @dictionary.keys.sort
  end
  
  def include? word
    @dictionary.has_key?(word)
  end
  
  def find word
    possibilities = {}
    
    possible_words = self.keywords.select { |keyword| keyword[0...word.length] == word }
    possible_words.each { |possible_word| possibilities.merge!(possible_word => @dictionary[possible_word]) }
    
    possibilities
    
  end
  
  def printable
    printout = @dictionary.sort.map do |word, definition|
      "[#{word}] " << "\"#{definition}\""
    end
    
    printout.join("\n")
    
  end
  
  
end