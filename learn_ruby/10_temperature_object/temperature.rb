class Temperature
    
  def initialize options
    if options[:f] 
      @f = options[:f]
      @c = ftoc(@f)
    end
    
    if options[:c] 
      @c = options[:c]
      @f = ctof(@c)
    end
  end
  
  def in_fahrenheit
    @f || ctof(@c)
  end

  def in_celsius
    @c || ftoc(@f)
  end
  
  def self.from_celsius cel
    Temperature.new(:c => cel)
  end
  
  def self.from_fahrenheit cel
    Temperature.new(:f => cel)
  end
  
  def ftoc f
    (f - 32) * 5/9.0
  end

  def ctof c
    c * 9/5.0 +32
  end
  
end


class Celsius < Temperature
  def initialize temp
    @c = temp
  end
end

class Fahrenheit < Temperature
  def initialize temp
    @f = temp
  end
end