class Book
  attr_accessor :title
  
  def title
    special_words = ['and', 'in', 'the', 'of', 'a', 'an']
    @title.capitalize.split(' ').map { |word| special_words.include?(word) ? word : word.capitalize }.join(' ')
  end
    
end