class RPNCalculator

  attr_accessor :numbers
  
  def initialize
    @numbers = []
  end

  def push num
    @numbers << num
  end

  def allowed_pop
    unless @numbers.length < 1
      return @numbers.pop
    else
      raise "calculator is empty"
    end
  end

  def plus
    @numbers << (allowed_pop + allowed_pop)
  end

  def minus
    @numbers << ((allowed_pop - allowed_pop) * -1)
  end  
  
  def divide
    @numbers << ((allowed_pop / allowed_pop.to_f) ** -1)
  end  

  def times
    @numbers << (allowed_pop * allowed_pop)
  end  
  
  def value
    @numbers[@numbers.length-1]
  end
  
  
  def tokens str
    str = str.split(' ')
    str.map do |item|
      if (('1'..'9').to_a).include?(item[0])
        item.to_f
      else
        item.to_sym
      end
    end
  end

  def evaluate str
    tokenized = tokens(str)
    
    tokenized.each do |token|
      if token.is_a?(Float)
        push token
        
      else 
        case token
        when :+
          plus
        when :-
          minus
        when :/
          divide
        when :*
          times
        end
      end
    end
    
    value

  end
end
