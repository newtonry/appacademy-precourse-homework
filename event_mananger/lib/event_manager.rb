#Ryan von Kunes Newton
#Event Manager
#http://tutorials.jumpstartlab.com/projects/eventmanager.html#iteration:-clean-phone-numbers

require "csv"
require "sunlight/congress"
require "erb"

Sunlight::Congress.api_key = "e179a6973728c4dd3fb1204283aaccb5"

def clean_zipcode zipcode
  zipcode.to_s.rjust(5,"0")[0..4]
end
  
def legislators_by_zipcode zipcode  
  Sunlight::Congress::Legislator.by_zipcode(zipcode)
end

def save_thank_you_letters(id, form_letter)
  Dir.mkdir("output") unless Dir.exists? "output"
  
  filename = "output/thanks_#{id}.html"
  File.open(filename, 'w') do |file|
    file.puts form_letter
  end
end


#Iteration: Clean Phone Numbers
def clean_phone_number phone_number
  number = phone_number.scan(/[0-9]/).join
  
  if number.length == 10
    number
  elsif (number.length == 11 and number[0] == '1')
    number[1...number.length]
  else
    nil
  end
end
  
#Iteration: Time Targeting
def most_frequent_hour dates
  hours = dates.map { |date| date.hour }
  freq_hour = hours.max_by { |hour| hours.count(hour) }
end

#Iteration: Day of the Week Targetting
def most_frequent_day dates
  days = dates.map { |date| date.to_date.wday }
  freq_day = days.max_by {|day| days.count(day)}
end


puts "EventManager initialized."

contents = CSV.open "event_attendees.csv", headers: true, header_converters: :symbol

template_letter = File.read "form_letter.erb"
erb_template = ERB.new template_letter

reg_dates = []
hours = []


contents.each do |row|
  id = row[0]
  
  name = row[:first_name]
  zipcode = clean_zipcode (row[:zipcode])
  
  legislators = legislators_by_zipcode(zipcode)
  phone_number = clean_phone_number(row[:homephone]) if row[:homephone].nil? == false
  
  reg_dates << DateTime.strptime(row[:regdate], '%m/%d/%y %H:%M')
    
  form_letter = erb_template.result(binding)
  
  save_thank_you_letters(id, form_letter)
end

puts "The most frequent hour during which people register is hour #{most_frequent_hour reg_dates}"
puts "People most often register on day #{most_frequent_day reg_dates} of the week"
